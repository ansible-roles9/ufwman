# [1.4.1](/Ansible_Roles/ufwman/compare/1.4.1...master)
* Fix `CUPS - WS-Discovery` port declaration format

# [1.4.0](/Ansible_Roles/ufwman/compare/1.4.0...master)
* CICD migraton from Jenkins to GitlabCI
* Adding new ports for `CUPS - WS-Discovery` service

# [1.3.0](/Ansible_Roles/ufwman/compare/1.3.0...master)
* Se configuran las versiones del `CHANGELOG.md` para que sean revisables por la herramienta de comparación de **Gitea**
* Se actualiza el *Jenkinsfile* para con las variables que son necesarias por la versión actual del **Shared libraries**

# [1.2.0](/Ansible_Roles/ufwman/compare/1.2.0...master)
* Se simplifican los pipelines usando los estándares segun tipologia, para el caso `ansiblePipeline`
* Se ajusta el `README` para que muestre información relevante
* Se ajusta el `.editorconfig`, `.gitignore` con las nuevas reglas bajo el estándar de plantilla

# [1.1.0](/Ansible_Roles/ufwman/compare/1.1.0...master)
* Se ajusta un detalle menor en el directorio **helpers** con los detalles del proyecto de acuerdo a la versión más reciente de `cicdHelpers`

# [1.0.0](/Ansible_Roles/ufwman/compare/1.0.0...master)
* Primera versión del rol `ufwman`
