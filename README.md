[![pipeline status](https://gitlab.com/ansible-roles9/ufwman/badges/development/pipeline.svg)](https://gitlab.com/ansible-roles9/ufwman/-/commits/development) ![Project version](https://img.shields.io/gitlab/v/tag/ansible-roles9/ufwman)![Project license](https://img.shields.io/gitlab/license/ansible-roles9/ufwman)

Role ufwman
===================

Description
-------------
The target of this role is allow to `configure easily` the firewall rules over linux distributions `that are using UFW`.

Requirements
-------------
It has no special requirements

How to use
-------------
    - import_role:
        name: "ufwman"
      vars:
        rules:
          - {source: "any or [source_ip]", port: "[port]", protocol: [valid_protocol], comment: "[comments]"}

ROOT Vars
-------------

* Variable name: `rules`
* Default value: empty [].
* Accepted values: Dictionaries list [key => value].
* Description: List with desired rules to implement in the server.

CHILD Vars for: `source`
-------------

* Variable name: `source`
* Default value: `NULL`.
* Accepted values: VALID IP/NETWORK or any
* Description: Valid IP address format or you can fill with a `any` string.

----------

CHILD Vars for: `port`
-------------

* Variable name: `port`
* Default value: `NULL`.
* Accepted values: INT
* Description: The por number that you want to allow traffic taking your server as destination.

----------

CHILD Vars for: `protocol`
-------------

* Variable name: `protocol`
* Default value: `tcp`.
* Accepted values: tcp / udp
* Description: The protocol that you want to allow, this argument is linked to `port`, if not set, **tcp** will use instead.

----------

CHILD Vars for: `comment`
-------------

* Variable name: `comment`
* Default value: `NULL`.
* Accepted values: STRING
* Description: Feel free to describe why you are creating this rule.


Legend
-------------
* NKS => No key sensitive
* KS => Key sensitive
